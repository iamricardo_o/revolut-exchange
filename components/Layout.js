import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Content,
  Text,
  StyleProvider,
  Button,
  Header,
  Left,
  Icon,
  Title,
  Body,
  Right,
  Footer,
  FooterTab,
  View
} from 'native-base';
import { Constants } from 'expo';
import styled from 'styled-components';

const StyledContent = styled(Content)`
  background-color: #fff;
  border: 1px solid red;
  flex: 1;
  height: 100%;
`;

const Layout = ({ children }) => {
  return (
    <Container
      style={{
        marginTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        position: 'relative'
      }}
    >
      <Header>
        <Left>
          <Button transparent>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>Header</Title>
        </Body>
        <Right />
      </Header>
      <StyledContent>{children}</StyledContent>;
    </Container>
  );
};

Layout.propTypes = {};

export default Layout;
