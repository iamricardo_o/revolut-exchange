import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'native-base';

const TextInput = ({ placeholder, value, prefix, keyboardType, ...props }) => {
  return <Input {...props} value={`${prefix} ${value}`} />;
};

TextInput.propTypes = {
  prefix: PropTypes.string,
  value: PropTypes.string
};

TextInput.defaultProps = {
  prefix: '',
  value: ''
};

export default TextInput;
