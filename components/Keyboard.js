import React, { Component } from 'react';
import { Keyboard as Base, TextInput } from 'react-native';

export default class Keyboard extends Component {
  //   componentDidMount() {
  //     this.keyboardDidShowListener = Base.addListener(
  //       'keyboardDidShow',
  //       this._keyboardDidShow
  //     );
  //     this.keyboardDidHideListener = Base.addListener(
  //       'keyboardDidHide',
  //       this._keyboardDidHide
  //     );
  //   }

  //   componentWillUnmount() {
  //     this.keyboardDidShowListener.remove();
  //     this.keyboardDidHideListener.remove();
  //   }

  //   _keyboardDidShow() {
  //     alert('Keyboard Shown');
  //   }

  //   _keyboardDidHide() {
  //     alert('Keyboard Hidden');
  //   }

  render() {
    return (
      <TextInput
        autoFocus
        behaviour="padding"
        onSubmitEditing={Keyboard.dismiss}
      />
    );
  }
}
