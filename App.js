import React from 'react';
import { StyleSheet, View } from 'react-native';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/commonColor';
import {
  Container,
  Content,
  Text,
  StyleProvider,
  Button,
  Header,
  Left,
  Icon,
  Title,
  Body,
  Right,
  Footer,
  FooterTab
} from 'native-base';
import commonColor from './native-base-theme/variables/commonColor';
import Expo from 'expo';
import { createStackNavigator } from 'react-navigation';
import ExchangeScreen from './containers/ExchangeScreen';
import Layout from './components/Layout';

const AppNavigator = createStackNavigator(
  {
    HomeScreen: {
      screen: ExchangeScreen,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'HomeScreen'
  }
);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false
    };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      Ionicons: require('@expo/vector-icons/fonts/Ionicons.ttf')
    });

    this.setState({ isReady: true });
  }

  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    }

    return (
      <StyleProvider style={getTheme(commonColor)}>
        <AppNavigator />
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
