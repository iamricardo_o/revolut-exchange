const BASE_URL = 'http://data.fixer.io/api';
const ACCESS_KEY = 'c2b16883c921b5f50004d81edd7a84c8';

/**
 * Returns exchange rates for a given currency
 */
const getRates = async currency => {
  const url = `${BASE_URL}/latest?access_key=${ACCESS_KEY}&base=${currency}`;
  const response = await fetch(url);

  if (!response.ok) {
    console.log(response.Error);
  }

  const data = await response.json();

  return data;
};

export default {
  getRates
};
