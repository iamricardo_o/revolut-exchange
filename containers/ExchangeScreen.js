import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Text,
  StyleProvider,
  Button,
  Header,
  Left,
  Icon,
  Title,
  Body,
  Right,
  Footer,
  FooterTab,
  View,
  Badge,
  Picker,
  Form
} from 'native-base';
import { Constants } from 'expo';
import styled, { css } from 'styled-components';
import { TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import Keyboard from '../components/Keyboard';
import TextInput from '../components/TextInput';
import fxService from '../services/fxService';

const lightGrey = '#f4f4f4';
const darkGrey = '#F0F0F0';
const secondaryColor = '#0075eb';

const Content = styled(KeyboardAvoidingView)`
  background-color: #fff;
  flex-direction: column;
  flex: 1;
  position: relative;
`;

const StyledHeader = styled(Header)`
  justify-content: space-between;
`;

const TrendingUpIcon = styled(Icon)`
  color: #fff;
  font-size: 24;
  position: relative;
  left: -5;
  top: 5;
`;

const Circle = styled.View`
  border-radius: 32;
  width: 32;
  height: 32;
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : '#000'};
  justify-content: center;
  align-items: center;
`;

const Top = styled.View`
  background-color: #fff;
  flex: 1;
  height: 50%;
  padding: 32px 16px;
`;
const Bottom = styled.View`
  background-color: ${lightGrey};
  flex: 1;
  flex-direction: column;
  height: 50%;
  padding: 32px 16px;
  justify-content: space-between;
  z-index: -1;
`;

const CurrencyCheckButton = styled(TouchableOpacity)`
  border: 1px solid ${darkGrey};
  width: auto;
  height: 32px;
  padding: 0px 8px;
  background: #fff;
  align-self: center;
  justify-content: space-between;
  align-items: center;
  border-radius: 32;
  flex-direction: row;
`;

const EqualityText = styled(Text)`
  color: ${secondaryColor};
`;

const Center = styled.View`
  position: relative;
`;

const CenterAbsoluteContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
`;

const border = () => css`
  border: 1px solid ${darkGrey};
`;

const backgroundColor = props => {
  return css`
    background-color: ${props.backgroundColor};
  `;
};

const CircleButton = styled.TouchableOpacity`
  background-color: #fff;
  ${props => props.backgroundColor && backgroundColor};
  ${props => props.border && border};
  border-radius: 32;
  width: 32;
  height: 32;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

const SwapButton = styled(CircleButton)`
  top: 50%;
  left: 16;
  position: absolute;
  z-index: 20;
`;

const CenterView = styled.View`
  justify-content: center;
  align-items: center;
  flex-direction: row;
  width: 100%;
  position: relative;
`;

const BlueIcon = styled(Icon)`
  color: ${secondaryColor};
  font-size: 14;
`;

const ExchangeButton = styled(Button)`
  z-index: 10;
  position: relative;
`;

const Row = styled.View`
  justify-content: space-between;
  flex-direction: row;
`;

class ExchangeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currencyTop: '£',
      currencyBottom: '€',
      valueTop: '0',
      valueBottom: '0'
    };
  }

  async componentDidMount() {
    const data = await fxService.getRates('EUR');
    console.log(data);
  }

  handleCurrencyPickerChange = value => {};

  render() {
    const { children } = this.props;
    const { currencyTop, currencyBottom, valueTop, valueBottom } = this.state;
    return (
      <Container
        style={{
          backgroundColor: '#fff',
          position: 'relative'
        }}
      >
        <Header noShadow>
          <Left>
            <Button transparent>
              <Icon name="close" />
            </Button>
          </Left>
          <Body>
            <Title>Exchange</Title>
          </Body>
          <Right>
            <CircleButton backgroundColor="black">
              <TrendingUpIcon name="trending-up" />
            </CircleButton>
          </Right>
        </Header>
        <Content behavior="padding">
          <Top>
            <Form>
              <Row>
                <Picker
                  renderHeader={backAction => (
                    <Header>
                      <Left>
                        <Button transparent onPress={backAction}>
                          <Icon name="close" />
                        </Button>
                      </Left>
                      <Body style={{ flex: 3 }}>
                        <Title>Choose Currency</Title>
                      </Body>
                      <Right />
                    </Header>
                  )}
                  mode="dropdown"
                  iosIcon={<Icon name="ios-arrow-down-outline" />}
                  selectedValue={currencyBottom}
                  onValueChange={this.handleCurrencyPickerChange}
                >
                  <Picker.Item label="GBP" value="€" />
                  <Picker.Item label="EUR" value="key1" />
                  <Picker.Item label="USD" value="key2" />
                  <Picker.Item label="Credit Card" value="key3" />
                  <Picker.Item label="Net Banking" value="key4" />
                </Picker>
                <TextInput
                  placeholder={valueTop}
                  autoFocus
                  keyboardType="number-pad"
                />
              </Row>
            </Form>
          </Top>
          <Center>
            <CenterAbsoluteContainer>
              <CenterView>
                <SwapButton
                  border
                  style={{
                    transform: [{ rotate: '90deg' }, { translateX: -15 }]
                  }}
                >
                  <BlueIcon name="swap" />
                </SwapButton>
                <CurrencyCheckButton rounded small>
                  <BlueIcon name="trending-up" style={{ marginRight: 8 }} />
                  <EqualityText>£1 = €2000,000.00</EqualityText>
                </CurrencyCheckButton>
              </CenterView>
            </CenterAbsoluteContainer>
          </Center>
          <Bottom>
            <Form>
              <Row>
                <Picker
                  renderHeader={backAction => (
                    <Header>
                      <Left>
                        <Button transparent onPress={backAction}>
                          <Icon name="close" />
                        </Button>
                      </Left>
                      <Body style={{ flex: 3 }}>
                        <Title>Choose Currency</Title>
                      </Body>
                      <Right />
                    </Header>
                  )}
                  mode="dropdown"
                  iosIcon={<Icon name="ios-arrow-down-outline" />}
                  selectedValue={currencyBottom}
                  onValueChange={this.handleCurrencyPickerChange}
                >
                  <Picker.Item label="GBP" value="€" />
                  <Picker.Item label="EUR" value="key1" />
                  <Picker.Item label="USD" value="key2" />
                  <Picker.Item label="Credit Card" value="key3" />
                  <Picker.Item label="Net Banking" value="key4" />
                </Picker>
                <TextInput
                  placeholder={valueBottom}
                  prefix="+"
                  keyboardType="number-pad"
                  value={50}
                />
              </Row>
            </Form>
            <ExchangeButton
              primary
              rounded
              block
              onPress={() => console.log('hello world')}
            >
              <Text>Exchange</Text>
            </ExchangeButton>
          </Bottom>
        </Content>
      </Container>
    );
  }
}

ExchangeScreen.propTypes = {};

export default ExchangeScreen;
